const puppeteer = require('puppeteer');
const uuidv4 = require('uuid/v4');

let testingURLtoPDF = async (html, callback, options = null, puppeteerArgs=null, remoteContent=true) => {

    if (typeof html !== 'string') {
        throw new Error(
            'Invalid Argument: HTML expected as type of string and received a value of a different type. Check your request body and request headers.'
        );
    }


    let browser = await puppeteer.launch({ headless: false });
    let page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 1080 });
    await page.setRequestInterception(true);

    page.on('request', (req) => {
        if(req.resourceType() === 'imagef'){
            req.abort();
        }
        else {
            req.continue();
        }
    });

    //await page.goto('https://citymap.tecnogeows.com/user/100690265898368600959/map/Waybh9ACo3YQYNpF1ifZRK');
    //await page.waitFor(10000);

    await page.goto(html, {
        waitUntil: 'load'
    });

    options = {
        path: uuidv4() + '.pdf', 
        landscape: false,
        margin: {
            top: '20px',
            right: '20px',
            bottom: '20px',
            left: '20px'
        },
        width: '860px',
        printBackground: true,
        scale: 1
    }

/*
    await page.pdf(options).then(callback, function(error) {
        console.log(error)
    });*/
    await page.close();
    await browser.close();

}


module.exports = testingURLtoPDF;
