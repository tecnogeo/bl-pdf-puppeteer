const puppeteer = require('puppeteer');
const uuidv4 = require('uuid/v4');
var request = require('request') // https://www.npmjs.com/package/request
    , async = require('async'); // https://www.npmjs.com/package/async

let testingURLtoPDF = async (html, callback, options = null, puppeteerArgs=null, remoteContent=true) => {

    if (typeof html !== 'string') {
        throw new Error(
            'Invalid Argument: HTML expected as type of string and received a value of a different type. Check your request body and request headers.'
        );
    }


    let browser = await puppeteer.launch({ headless: false });
    let page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 1080 });
    await page.setRequestInterception(true);

    page.on('request', (req) => {
        if(req.resourceType() === 'imagef'){
            req.abort();
        }
        else {
            req.continue();
        }
    });

    //await page.goto('https://citymap.tecnogeows.com/user/100690265898368600959/map/Waybh9ACo3YQYNpF1ifZRK');
    //await page.waitFor(10000);

    await page.goto(html, {
        waitUntil: 'networkidle0'
    });

    options = {
        path: uuidv4() + '.pdf', 
        landscape: false,
        margin: {
            top: '20px',
            right: '20px',
            bottom: '20px',
            left: '20px'
        },
        width: '860px',
        printBackground: true,
        scale: 1
    }

/*
    await page.pdf(options).then(callback, function(error) {
        console.log(error)
    });*/
    await page.close();
    await browser.close();

}

let initStressTest = async (testNumbers, callback) => {
    let myUrls = [];
    console.log(testNumbers);
    for (let i = 0; i < testNumbers; i++) {
        myUrls.push("https://citymap.tecnogeows.com/user/100677080940829449091/map/N9kAm6RsieKuwtvDuPMis6?zoom=16&center=41.3989,2.1892");
    }

    console.log(myUrls.length);
    console.log(JSON.stringify(myUrls));
    await loadTest(myUrls);
}



let loadTest = async (myUrls) =>

async.map(myUrls, function(url, callback) {
    /*request(url, function(error, response, html) {
      // Some processing is happening here before the callback is invoked



      console.log(response);
      callback(error, html);
    });*/

    
        testingURLtoPDF(url, pdf => {
            console.log("loaded!")
            //res.setHeader('Content-Type', 'application/pdf');
            //res.send(pdf);
        }, null, null, true).catch(err => {
            console.log(err);
            //res.status(500).send('An error occurred');
        });

  }, function(err, results) {
    console.log(err);
    console.log(results);
  });

module.exports = testingURLtoPDF;
