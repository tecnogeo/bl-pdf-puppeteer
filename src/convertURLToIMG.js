const puppeteer = require('puppeteer');
const uuidv4 = require('uuid/v4');

let convertURLtoIMG = async (html, callback, options = null, puppeteerArgs=null, remoteContent=true) => {

    if (typeof html !== 'string') {
        throw new Error(
            'Invalid Argument: HTML expected as type of string and received a value of a different type. Check your request body and request headers.'
        );
    }
    

    const browser = await puppeteer.launch()
    const page = await browser.newPage()
    await page.goto(html, {
        waitUntil: 'networkidle0'
    });
    
    //if (!options) {
        options = {path: uuidv4() + '.png', 
        fullPage: true,
        margin: {
            top: '20px',
            right: '20px',
            bottom: '20px',
            left: '20px'
        },
        type: 'png',
        omitBackground: false,
        scale: 1
        }
    //}

    //await page.pdf({path: 'medium.pdf', format: 'A4'})

    await page.screenshot(options).then(callback, function(error) {
        console.log(error)
    })

    await browser.close()
}


module.exports = convertURLtoIMG;
