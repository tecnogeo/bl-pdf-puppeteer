const puppeteer = require('puppeteer');
const uuidv4 = require('uuid/v4');
const path = require('path');


let convertURLtoPDF = async (html, callback, options = null, puppeteerArgs=null, remoteContent=true) => {

    if (typeof html !== 'string') {
        throw new Error(
            'Invalid Argument: HTML expected as type of string and received a value of a different type. Check your request body and request headers.'
        );
    }
    
    const isPkg = typeof process.pkg !== 'undefined';
    
    let chromiumExecutablePath = puppeteer.executablePath();
    if(isPkg) {
        chromiumExecutablePath = chromiumExecutablePath.substring(chromiumExecutablePath.indexOf('.local-chromium\\')).replace('.local-chromium', 'chromium');
        chromiumExecutablePath = path.join(process.cwd(), chromiumExecutablePath);
    }

    const browser = await puppeteer.launch({
        executablePath: chromiumExecutablePath
    })
    const page = await browser.newPage()
    await page.goto(html, {
        waitUntil: 'networkidle0',
        timeout: 0
    });

    //await page.setViewport({width: 794, height: 1122, deviceScaleFactor: 1});
    
    //if (!options) {
        options = {
            // path: uuidv4() + '.pdf', 
            landscape: false,
            margin: {
                top: '40px',
                right: '20px',
                bottom: '20px',
                left: '20px'
            },
            width: '21cm',
            printBackground: true,
            scale: 1
        }
    //}

    await page.pdf(options).then(callback, function(error) {
        console.log(error)
    })

    await browser.close()
}


module.exports = convertURLtoPDF;
