const express = require('express');
// const puppeteer = require('puppeteer');
const bodyParser = require('body-parser');
const http = require('http');
const convertHTMLToPDF = require('./src/convertHTMLToPDF');
const convertURLtoPDF = require('./src/convertURLtoPDF');
const convertURLtoIMG = require('./src/convertURLtoIMG');
process.setMaxListeners(Infinity); // <== Important line
const app = express();
const router = express.Router();

const isPkg = typeof process.pkg !== 'undefined';
if (isPkg) {
    process.title = 'My title';
}

// Example PDF route, can be used with the postman profile
// to test different option configurations, for example:
// changing the remoteContent option to false will allow the "GiantPDF"
// to load faster, but without the remote images.
router.route('/htmlpdf').post(async function(req, res) {
    convertHTMLToPDF(req.body, pdf => {
        res.setHeader('Content-Type', 'application/pdf');
        res.send(pdf);
    }, null, null, true).catch(err => {
        console.log(err);
        res.status(500).send({error: err});
    });
});

// Example PDF route, can be used with the postman profile
// to test different option configurations, for example:
// changing the remoteContent option to false will allow the "GiantPDF"
// to load faster, but without the remote images.
router.route('/urlpdf').post(async function(req, res) {
    console.log("urlpdf sense query")
    convertURLtoPDF(req.body, pdf => {
        res.setHeader('Content-Type', 'application/pdf');
        res.send(pdf);
    }, null, null, true).catch(err => {
        console.log(err);
        res.status(500).send({error: err});
    });
});

// Example PDF route, can be used with the postman profile
// to test different option configurations, for example:
// changing the remoteContent option to false will allow the "GiantPDF"
// to load faster, but without the remote images.
router.route('/urlimg').post(async function(req, res) {
    convertURLtoIMG(req.body, img => {
        res.setHeader('Content-Type', 'image/png');
        res.send(img);
    }, null, null, true).catch(err => {
        console.log(err);
        res.status(500).send({error: err});
    });
});

// Example PDF route, can be used with the postman profile
// to test different option configurations, for example:
// changing the remoteContent option to false will allow the "GiantPDF"
// to load faster, but without the remote images.
router.route('/urlpdf').get(async function(req, res) {
    const query = req.query;
    console.log("urlpdf amb query", query)

    if (!query || !query.url) {        
        res.status(500).send('An error occurred. Parameter "query" is null');
    }

    await convertURLtoPDF(query.url, pdf => {
        res.setHeader('Content-Type', 'application/pdf');
        res.send(pdf);
    }, null, null, true).catch(err => {
        console.log(err);
        res.status(500).send({error: err});
    });
});

// Example IMG route, can be used with the postman profile
// to test different option configurations, for example:
// changing the remoteContent option to false will allow the "GiantIMG"
// to load faster, but without the remote images.
router.route('/urlimg').get(async function(req, res) {

    const query = req.query;

    if (!query || !query.url) {
        res.status(500).send('An error occurred');
    }

    convertURLtoIMG(query.url, img => {
        res.setHeader('Content-Type', 'image/png');
        res.send(img);
    }, null, null, true).catch(err => {
        console.log(err);
        res.status(500).send({error: err});
    });
});

// Test route
router.route('/ping').get(async function(req, res) {
    res.send('Hello World');
});

app.use(
    bodyParser.text({
        limit: '50mb'
    })
);

app.use('/api', router);

// Start the server.
var port = 3000;
http.createServer(app).listen(port);
console.log('Server listening on port ' + port);
